﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CameraPro
{
    public class PluginInfos
    {
        public const string PLUGIN_ID = "com.gitlab.axolotlll.CameraPro";
        public const string PLUGIN_NAME = "CameraPro";
        public const string PLUGIN_VERSION = "1.0.0";
    }
}
