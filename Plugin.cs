﻿using BepInEx;
using BepInEx.Configuration;
using GameplayEntities;
using HarmonyLib;
using LLBML.Math;
using LLBML.Utils;
using LLHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using UnityEngine;

namespace CameraPro
{
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        public static Plugin instance;
        public ConfigEntry<bool> isModEnabled;
        public List<Cam> cameras;
        public int activeIndex = 0;
        public bool autoDirector = true;
        float StageMaxX => new Floatf(World.instance.stageMax.GCPKPHMKLBN);
        
        void Awake()
        {
            instance = this;

            var harmony = new Harmony(PluginInfos.PLUGIN_ID);
            harmony.PatchAll();

            InitConfig();
            cameras = new List<Cam>();

            float phaseShift = 0.5f;
            float pOH_height = 6f;
            float pOH_dist = 7f;
            float pOH_distMod = 0.1f;
            float pOH_speed = 60f;
            float pOH_stayPercent = 0.1f;
            cameras.Add(new Cam("PanningOH", (time, transform) => {
                float x = Mathf.Clamp(Mathf.Sin(2 * Mathf.PI * time / pOH_speed), -1 + pOH_stayPercent, 1 - pOH_stayPercent);
                float dz = Mathf.Abs(Mathf.Clamp(Mathf.Cos(2 * Mathf.PI * time / pOH_speed), -1 + pOH_stayPercent, 1 - pOH_stayPercent));
                Vector3 pos = new Vector3(StageMaxX * x, pOH_height, -pOH_dist - pOH_distMod * dz);
                transform.position = pos;
                float swivel = x * -5;
                transform.rotation = Quaternion.Euler(new Vector3(20, swivel, 0));
                return true;
            }));

            float pC_height = 0.6f;
            float pC_dist = 5f;
            float pC_distMod = 0.2f;
            float pC_speed = 120f;
            float pC_stayPercent = 0f;
            cameras.Add(new Cam("PanningClose", (time, transform) => {
                float x = Mathf.Clamp(Mathf.Sin(-2 * Mathf.PI * time / pC_speed), -1 + pC_stayPercent, 1 - pC_stayPercent);
                float dz = Mathf.Abs(Mathf.Clamp(Mathf.Cos(2 * Mathf.PI * time / pC_speed), -1 + pC_stayPercent, 1 - pC_stayPercent));
                Vector3 pos = new Vector3(0.8f * StageMaxX * x, pC_height, -pC_dist - pC_distMod * dz);
                transform.position = pos;
                float swivel = x * -15;
                transform.rotation = Quaternion.Euler(new Vector3(-10, swivel, 0));
                return true;
            }));

            float bG_dist = -4f;
            float bG_height = 2.5f;
            Quaternion forwards = Quaternion.Euler(0, 0, 0);
            cameras.Add(new Cam("BallGimbal", (time, transform) => {
                transform.position = new Vector3(0, bG_height, -5f);
                if (BallHandler.instance.GetBall(0) != null)
                {
                    //Quaternion newRot = Quaternion.LookRotation(BallHandler.instance.GetBall(0).transform.position);
                    if (BallHandler.instance.GetBall(0).IsActivelyInMatch())
                    {
                        transform.LookAt(BallHandler.instance.GetBall(0).transform);
                        return true;
                    }
                    else if (ItemHandler.instance.corpseItems[0] != null)
                    {
                        Logger.LogInfo("Corpse 0 not-null");
                        if (ItemHandler.instance.corpseItems[0].itemData.active)
                        {
                            Logger.LogInfo("Corpse 0 lookat");
                            transform.LookAt(ItemHandler.instance.corpseItems[0].transform);
                            return true;
                        }
                    }
                    else if (ItemHandler.instance.corpseItems[1] != null)
                    {
                        Logger.LogInfo("Corpse 1 not-null");
                        if (ItemHandler.instance.corpseItems[1].itemData.active)
                        {
                            Logger.LogInfo("Corpse 1 lookat");
                            transform.LookAt(ItemHandler.instance.corpseItems[1].transform);
                            return true;
                        }
                    }
                    //transform.rotation = Quaternion.Lerp(forwards, newRot, 0.5f);
                }
                
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                
                
                return true;
            }));

            float minDist = 3f;
            float distPlier = 0.7f;
            float sigMax = 2f;
            Quaternion camLook = Quaternion.Euler(7, 0, 0);
            cameras.Add(new Cam("FrontAction", (time, transform) => {
                Vector3 ballPos = new Vector3(0, 2, 0);
                if (BallHandler.instance.GetBall(0) != null)
                {
                    ballPos = BallHandler.instance.GetBall(0).transform.position;
                }
                Vector2f p1Pos = new Vector2f(0, 1);
                if (PlayerHandler.instance.GetPlayerEntity(0) != null)
                {
                    p1Pos = PlayerHandler.instance.GetPlayerEntity(0).GetPosition();
                }
                Vector2f p2Pos = new Vector2f(0, 1);
                if (PlayerHandler.instance.GetPlayerEntity(1) != null)
                {
                    p2Pos = PlayerHandler.instance.GetPlayerEntity(1).GetPosition();
                }

                float x = (ballPos.x + p1Pos.x + p2Pos.x) / 3f;
                float sigX = Mathf.Pow(ballPos.x - x, 2) + Mathf.Pow(p1Pos.x - x, 2) + Mathf.Pow(p2Pos.x - x, 2);
                float sigX2 = sigX / 3;

                float y = (ballPos.y + p1Pos.y + p2Pos.y) / 3f;
                float sigY = Mathf.Pow(ballPos.y - y, 2) + Mathf.Pow(p1Pos.y - y, 2) + Mathf.Pow(p2Pos.y - y, 2);
                float sigY2 = sigY / 3;

                float sigXY = Mathf.Clamp(Mathf.Sqrt(sigX2 + sigY2), 0, sigMax);

                Vector3 camPos = new Vector3(x, y + 0.5f, -minDist - sigXY * distPlier);
                transform.SetPositionAndRotation(camPos, camLook);
                return true;
            }));

            float minDist2 = 7f;
            float distPlier2 = 0.7f;
            float sigMax2 = 2f;
            Quaternion camLook2 = Quaternion.Euler(5, 0, 0);
            cameras.Add(new Cam("DeepAction", (time, transform) => {
                Vector3 ballPos = new Vector3(0, 2, 0);
                if (GetBall() != null)
                {
                    ballPos = GetBall().transform.position;
                }
                Vector2f p1Pos = new Vector2f(0, 1);
                if (GetP1() != null)
                {
                    p1Pos = GetP1().GetPosition();
                }
                Vector2f p2Pos = new Vector2f(0, 1);
                if (GetP2() != null)
                {
                    p2Pos = GetP2().GetPosition();
                }

                float x = (ballPos.x + p1Pos.x + p2Pos.x) / 3f;
                float sigX = Mathf.Pow(ballPos.x - x, 2) + Mathf.Pow(p1Pos.x - x, 2) + Mathf.Pow(p2Pos.x - x, 2);
                float sigX2 = sigX / 3;

                float y = (ballPos.y + p1Pos.y + p2Pos.y) / 3f;
                float sigY = Mathf.Pow(ballPos.y - y, 2) + Mathf.Pow(p1Pos.y - y, 2) + Mathf.Pow(p2Pos.y - y, 2);
                float sigY2 = sigY / 3;

                float sigXY = Mathf.Clamp(Mathf.Sqrt(sigX2 + sigY2), 0, sigMax2);

                Vector3 camPos = new Vector3(x / 5, (y / 5) + 1.5f, -minDist2 - sigXY * distPlier2);
                transform.SetPositionAndRotation(camPos, camLook2);
                return true;
            }));

            float spinFreq = 4f;
            float spinDist = 3.0f;
            float spinSigPlier = 0.25f;
            cameras.Add(new Cam("SpinningAction", (time, transform) =>
            {
                Vector3 ballPos = new Vector3(0, 2, 0);
                if (BallHandler.instance.GetBall(0) != null)
                {
                    ballPos = BallHandler.instance.GetBall(0).transform.position;
                }
                Vector2f p1Pos = new Vector2f(0, 1);
                if (PlayerHandler.instance.GetPlayerEntity(0) != null)
                {
                    p1Pos = PlayerHandler.instance.GetPlayerEntity(0).GetPosition();
                }
                Vector2f p2Pos = new Vector2f(0, 1);
                if (PlayerHandler.instance.GetPlayerEntity(1) != null)
                {
                    p2Pos = PlayerHandler.instance.GetPlayerEntity(1).GetPosition();
                }

                float x = (ballPos.x + p1Pos.x + p2Pos.x) / 3f;
                float sigX = Mathf.Pow(ballPos.x - x, 2) + Mathf.Pow(p1Pos.x - x, 2) + Mathf.Pow(p2Pos.x - x, 2);
                float sigX2 = sigX / 3;

                float y = (ballPos.y + p1Pos.y + p2Pos.y) / 3f;
                float sigY = Mathf.Pow(ballPos.y - y, 2) + Mathf.Pow(p1Pos.y - y, 2) + Mathf.Pow(p2Pos.y - y, 2);
                float sigY2 = sigY / 3;

                float sigXY = Mathf.Clamp(Mathf.Sqrt(sigX2 + sigY2), 0, sigMax);
                float newSpinDist = spinDist + sigXY * spinSigPlier;
                Vector3 unitCirc = new Vector3(Mathf.Sin(2 * Mathf.PI * time / spinFreq), 0, Mathf.Cos(2 * Mathf.PI * time / spinFreq));
                Vector3 camPos = new Vector3(x - newSpinDist * unitCirc.x, 1f + (0.25f * y), -newSpinDist * unitCirc.z);
                transform.position = camPos;
                transform.LookAt(new Vector3(x, y, 0));
                return true;
            }));
        }

        void Start()
        {
            ModDependenciesUtils.RegisterToModMenu(this.Info);

            for (int i = 1; i < Display.displays.Length; i++)
            {
                Display.displays[i].Activate();
            }
        }
        void InitConfig()
        {
            isModEnabled = Config.Bind<bool>("Toggles", "Enabled", true, "Enables / disables the mod.");
        }

        void FixedUpdate()
        {
            // auto director
            if (autoDirector)
            {
                BallEntity ball = BallHandler.instance.GetBall(0);
                PlayerEntity p1 = PlayerHandler.instance.GetPlayerEntity(0);
                PlayerEntity p2 = PlayerHandler.instance.GetPlayerEntity(1);
                
                if (ball == null || p1 == null || p2 == null)
                {
                    activeIndex = 0;
                }
                else
                {

                    float speed = new Floatf(ball.ballData.flySpeed);


                    if (ball.IsActivelyInMatch() && p1.IsActivelyInMatch() && p2.IsActivelyInMatch())
                    {

                        if (ball.ballData.ballState == GameplayEntities.BallState.HITSTUN)
                        {
                            float rem = new Floatf(ball.RemainingHitstunTime());
                            if (rem > 5f && (new Floatf(ball.ballData.hitstunDuration) > 20f ||
                                (speed > 80f && !ball.InBuntTypeStun())))
                            {
                                activeIndex = 5;
                            }
                        }
                        else if (ball.ballData.ballState == BallState.SERVE)
                        {
                            activeIndex = 2;
                        }
                        else if (speed < 30f)
                        {
                            activeIndex = 3;
                        }
                        else
                        {
                            if (Mathf.Floor(Time.time / 45f) % 2 == 0)
                            {
                                activeIndex = 1;
                            }
                            else
                            {
                                activeIndex = 4;
                            }

                        }
                    }
                    else
                    {
                        // is there corpses?
                        //if (ItemHandler.instance.CorpsesActive() > 0)
                        //{
                        //    activeIndex = 2; // corpse gimbal
                        //}
                        //else
                        //{
                            if (Mathf.Floor(Time.time / 75f) % 2 == 0)
                            {
                                activeIndex = 0;
                            }
                            else
                            {
                                activeIndex = 1;
                            }
                        //}
                        

                    }
                }
            }
            


            // get buttons
            if (Input.GetKey(KeyCode.Alpha1)) activeIndex = 0; // overhead pan
            else if (Input.GetKey(KeyCode.Alpha2)) activeIndex = 1; // close pan
            else if (Input.GetKey(KeyCode.Alpha3)) activeIndex = 2; // ball/corpse gimbal
            else if (Input.GetKey(KeyCode.Alpha4)) activeIndex = 3; // close action
            else if (Input.GetKey(KeyCode.Alpha5)) activeIndex = 4; // deep action
            else if (Input.GetKey(KeyCode.Alpha6)) activeIndex = 5; // spinning action
            

            //apply transforms
            for (int i = 0; i < cameras.Count; i++)
            {
                cameras[i].ApplyTransform();
                cameras[i].SetIsActive(i == activeIndex);
            }
            foreach (Cam cam in cameras)
            {
                cam.ApplyTransform();
            }
        }

        public void InitCameras()
        {
            foreach (Cam cam in cameras)
            {
                cam.Init();
            }
        }

        private static BallEntity GetBall()
        {
            if (BallHandler.instance == null) return null;
            return (BallHandler.instance.GetBall(0));
        }

        private static PlayerEntity GetP1()
        {
            if (PlayerHandler.instance == null) return null;
            return PlayerHandler.instance.GetPlayerEntity(0);
        }

        private static PlayerEntity GetP2()
        {
            if (PlayerHandler.instance == null) return null;
            return PlayerHandler.instance.GetPlayerEntity(1);
        }
        
        private static CorpseEntity GetCorpse1()
        {
            if (ItemHandler.instance == null) return null;
            if (ItemHandler.instance.corpseItems == null) return null;
            return ItemHandler.instance.corpseItems[0];
        }

        private static CorpseEntity GetCorpse2()
        {
            if (ItemHandler.instance == null) return null;
            if (ItemHandler.instance.corpseItems == null) return null;
            return ItemHandler.instance.corpseItems[1];
        }
    }

    [HarmonyPatch(typeof(World), nameof(World.Init1))]
    public class WorldInitPatch
    {
        public static void Postfix()
        {
            Plugin.instance.InitCameras();
        }
    }
}
