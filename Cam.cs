﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CameraPro
{
    public class Cam
    {
        private Func<float, Transform, bool> doTransform;
        private GameObject obj;
        private string name;

        public Cam(string name, Func<float, Transform, bool> doTransform)
        {
            this.name = name;
            SetAction(doTransform);
        }

        public void Init()
        {
            obj = new GameObject("cameraMod_" + name);
            obj.AddComponent<Camera>();
            obj.GetComponent<Camera>().targetDisplay = 1;
            obj.GetComponent<Camera>().cullingMask &= ~(1 << 9);

            //TODO: add streets obj
        }

        public void SetAction(Func<float, Transform, bool> doTransform)
        {
            this.doTransform = doTransform;
        }


        public void ApplyTransform()
        {
            doTransform(Time.time + 17f, obj.transform); // arbitrarily add a phase shift here, so we don't have to worry about syncing formulas
        }

        public void SetIsActive(bool isActive)
        {
            obj.SetActive(isActive);
        }

    }
}
